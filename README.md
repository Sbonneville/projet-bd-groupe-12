Déploiment de l'application :

Etape 1 : Allumer le vpn

Etape 2 : executer le fichier 'creation_tables.sql' qui créera les tables et contraintes
necessaires au fonctionnement de l'application.

Etape 3 : executer le fichier 'jeu_de_donnees.sql'.

/\ En cas d'erreur au niveau du sgbd il est possible d'executer 'suppression_tables.sql' qui supprimera les 
tables crées précédement, il faudra dans ce cas, recommencer depuis l'étape 2. /\

Etape 4 : executer le fichier app.jar, pour cela il faut saisir : "java -jar <chemin où se trouve le .jar>" dans une invite de commande
ou alors powershell selon le système d'exploitation. Une fois cela fait l'application demandera alors identifiants et mdp
afin de se connecter au sgbd. S'affichera par la suite un menu afin d'accéder aux différentes fonctionnalités.

Notes concernant la structure de l'aplication : 
- Les classes Vol, Reservation et Client ne contienne aucunes méthodes, juste les attributs, getter / setter et constructeur.
- La suppression d'un vol n'est pas disponible.
- Les requêtes sql (jdbc) se trouvent toutes dans les classes ReservationDAO et VolDAO
- Les méthodes de la classe main appellent les requêtes de ReservationDAO et VolDAO