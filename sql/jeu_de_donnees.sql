INSERT INTO Modele VALUES(1, 'Boeing', 4800, 2);

INSERT INTO Avion VALUES(1, 1);
INSERT INTO Avion VALUES(2, 1);
INSERT INTO Avion VALUES(3, 1);

INSERT INTO Place VALUES(1, 'hublot', 'premiere', 1);
INSERT INTO Place VALUES(2, 'centre', 'premiere', 1);
INSERT INTO Place VALUES(3, 'couloir', 'premiere', 1);

INSERT INTO Personne VALUES(1, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(2, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(3, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(4, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(5, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(6, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(7, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(8, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(9, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(10, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(11, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);
INSERT INTO Personne VALUES(12, 'nomTest', 'prenomTest', '24bis', 'rue lafayette', '75000', 'Paris', 'France', 5);

INSERT INTO Client VALUES(1, 0, 1);
INSERT INTO Client VALUES(2, 0, 2);

INSERT INTO Pilote VALUES(1, 'Paris', 3);
INSERT INTO Pilote VALUES(2, 'Paris', 4);
INSERT INTO Pilote VALUES(3, 'Lyon', 5);
INSERT INTO Pilote VALUES(4, 'Marseille', 6);

INSERT INTO EtreQualifie VALUES(1, 1);
INSERT INTO EtreQualifie VALUES(1, 2);
INSERT INTO EtreQualifie VALUES(1, 4);

INSERT INTO Hotesse VALUES(1, 3, 'Marseille', 7);
INSERT INTO Hotesse VALUES(2, 2, 'Marseille', 8);
INSERT INTO Hotesse VALUES(3, 3, 'Marseille', 9);
INSERT INTO Hotesse VALUES(4, 1, 'Marseille', 10);
INSERT INTO Hotesse VALUES(5, 4, 'Marseille', 11);
INSERT INTO Hotesse VALUES(6, 2, 'Marseille', 12);

INSERT INTO Vol VALUES(647, 'Paris Orly', 'Lyon St Exupery', to_date('07/08/2020','DD/MM/YYYY'), '13h54', 98, 0, 376, 1);

INSERT INTO Affecte VALUES(4, 647);
INSERT INTO Affecte VALUES(2, 647);

INSERT INTO Affecte VALUES(1, 647);

INSERT INTO Reservation VALUES(2647, to_date('21/03/2020','DD/MM/YYYY'), 70, 2, 647, 3);