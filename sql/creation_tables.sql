CREATE TABLE Vol (
numeroVol number(8) NOT NULL,
aeroportD varchar(200),
aeroportA varchar(200),
dateV date,
horaire varchar(200),
duree number(8),
terminaison number(1),
distance float(5),
numeroAvion number(8),
PRIMARY KEY (numeroVol));

CREATE TABLE Avion (
numeroAvion number(8) NOT NULL,
numeroM number(8),
PRIMARY KEY (numeroAvion));

CREATE TABLE Place (
numeroP number(8) NOT NULL,
position_Place varchar(200),
classe_Place varchar(200),
numeroAvion number(8),	
PRIMARY KEY (numeroP),
constraint ck_place_classe CHECK(classe_Place in ('eco', 'premiere', 'affaire')),
constraint ck_place_position check(position_Place in ('hublot', 'couloir', 'centre')));

CREATE TABLE Modele (
numeroM number(8) NOT NULL,
nomM VARCHAR(200),
rayonAction float(5),
nbPilotesNecessaires number(8),
PRIMARY KEY (numeroM));

CREATE TABLE Personne (
numeroPers number(8) NOT NULL,
nom varchar(200),
prenom varchar(200),
numero varchar(200),
rue varchar(200),
CP varchar(200),
ville varchar(200),
pays varchar(200),
nbHeuresDeVol number(8),
PRIMARY KEY (numeroPers));

CREATE TABLE Client (
numeroCli number(8) NOT NULL,
reduction number(8),
numeroPers number(8),
PRIMARY KEY (numeroCli));

CREATE TABLE Pilote (
numeroPilote number(8) NOT NULL,
localisation varchar(200),
numeroPers number(8),
PRIMARY KEY (numeroPilote));

CREATE TABLE Hotesse (
numeroHotesse number(8) NOT NULL,
nbLanguesParlees number(8),
localisation varchar(200),
numeroPers number(8),
PRIMARY KEY (numeroHotesse));

CREATE TABLE Reservation (
numeroRes number(10) NOT NULL,
dateRes DATE,
prix float(5),
numeroCli number(8),
numeroVol number(8),
numeroP number(8),
PRIMARY KEY (numeroRes));

CREATE TABLE EtreQualifie (
numeroM number(8) NOT NULL,
numeroPilote number(8) NOT NULL,
PRIMARY KEY (numeroM, numeroPilote));

CREATE TABLE Affecte (
numeroPilote number(8) NOT NULL,
numeroVol number(8) NOT NULL,
PRIMARY KEY (numeroPilote, numeroVol));

CREATE TABLE Affectee (
numeroHotesse number(8) NOT NULL,
numeroVol number(8) NOT NULL,
PRIMARY KEY (numeroHotesse, numeroVol));

ALTER TABLE Vol ADD CONSTRAINT FK_Vol_numeroAvion FOREIGN KEY (numeroAvion) REFERENCES Avion (numeroAvion);
ALTER TABLE Avion ADD CONSTRAINT FK_Avion_numeroM FOREIGN KEY (numeroM) REFERENCES Modele (numeroM);
ALTER TABLE Place ADD CONSTRAINT FK_Place_numeroAvion FOREIGN KEY (numeroAvion) REFERENCES Avion (numeroAvion);
ALTER TABLE Reservation ADD CONSTRAINT FK_Reservation_numeroCli FOREIGN KEY (numeroCli) REFERENCES Client (numeroCli);
ALTER TABLE Reservation ADD CONSTRAINT FK_Reservation_numeroVol FOREIGN KEY (numeroVol) REFERENCES Vol (numeroVol);
ALTER TABLE Reservation ADD CONSTRAINT FK_Reservation_numeroP FOREIGN KEY (numeroP) REFERENCES Place (numeroP);
ALTER TABLE EtreQualifie ADD CONSTRAINT FK_EtreQualifie_numeroM FOREIGN KEY (numeroM) REFERENCES Modele (numeroM);
ALTER TABLE EtreQualifie ADD CONSTRAINT FK_EtreQualifie_numeroPilote FOREIGN KEY (numeroPilote) REFERENCES Pilote (numeroPilote);
ALTER TABLE Affecte ADD CONSTRAINT FK_Affecte_numeroPilote FOREIGN KEY (numeroPilote) REFERENCES Pilote (numeroPilote);
ALTER TABLE Affecte ADD CONSTRAINT FK_Affecte_numeroVol FOREIGN KEY (numeroVol) REFERENCES Vol (numeroVol);
ALTER TABLE Affectee ADD CONSTRAINT FK_Affectee_numeroHotesse FOREIGN KEY (numeroHotesse) REFERENCES Hotesse (numeroHotesse);
ALTER TABLE Affectee ADD CONSTRAINT FK_Affectee_numeroVol FOREIGN KEY (numeroVol) REFERENCES Vol (numeroVol);
ALTER TABLE Client ADD CONSTRAINT FK_Client_Personne_numeroPers FOREIGN KEY (numeroPers) REFERENCES Personne (numeroPers);
ALTER TABLE Hotesse ADD CONSTRAINT FK_Hotesse_Personne_numeroPers FOREIGN KEY (numeroPers) REFERENCES Personne (numeroPers);
ALTER TABLE Pilote ADD CONSTRAINT FK_Pilote_Personne_numeroPers FOREIGN KEY (numeroPers) REFERENCES Personne (numeroPers);